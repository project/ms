/*
 * English to Malay
 * Software terms
 * By Abdullah Daud
 *
 * Updated 5/9/08 by ckng (refer resources)
 * - fixed existing terms
 * Updated 19/9/08 by ckng
 * - added new terms
 * - fixed ambiguous terms
 *
 * Useful resources:
 * - http://www.proz.com/?sp=ksearch
 * - http://sbmb.dbp.gov.my/knb/cadangankata/nb_cadangan_kata_istilah.aspx (Bidang Komputer, Teknologi Maklumat or Semua)
 * - http://sbmb.dbp.gov.my/knb/cariankata/nb_carian_kata.aspx
 * - http://www.citcat.com/green_ov.jsp?lang=1
 * - http://utmk.cs.usm.my:8080/ebmt-controller/servlet/EBMTServlet2 (Domain: Tactics)
 */

abort - henti paksa
access - akses
account - akaun
action - tindakan
active - aktif
addition - penambahan
adjust - melaraskan, menyesuaikan
address - alamat
admin - tadbir
administer - mentadbir
advanced - maju
affiliate - gabungan, ahli gabungan, ahli sekutu
affiliation - hubungan
aggregate - agregat
alias - alias
allow - membenarkan
alternative - alternatif, lain
ampersand - nampersan
anonymous - tanpa nama, tanpanama
applicable - berkenaan
application - aplikasi
appropriate - sesuai
approval - kelulusan, pengesahan
ascending - menaik
assignment - penetapan
associate - mengaitkan
attach - dilampir
attachment - lampiran
attribute - atribut
authentic - tulen
authenticate - mengesahkan, mengesahkan ketulenan
authenticated - disahkan
authentication - pengesahan
authoring - pengarangan
authorize - membenarkan
authorized - dibenarkan, diberi kuasa
autocomplete - automatik lengkap
await - menanti
backup file - fail sandar
basic - asas
batch file - fail kelompok
begin - mula
behaviour - kelakuan
block - blok (as in block area)
blocked - disekat, dihalang
blog - blog
blogger - bloger
body - kandungan, badan kandungan
built-in - terbina dalam, sedia ada
browser - pelayar
by default - dengan lalai
byte - bait
cancel - batal
capture - penangkapan
case - kes
category - kategori
character - aksara
check - semakan
check - kotak (as in checkbox)
checkbox - kotak semak
chronological - kronologi
class - kelas
classify - mengelaskan, mengklasifikasikan
click - klik
collaborate - bekerjasama
collapsed - terlipat
combine - menggabungkan, merangkaikan
compile - kompil
completed - disempurnakan
compose - mengarang
configure - konfigurasikan, lakukan konfigurasi, mengkonfigurasi
configuration - konfigurasi, tatarajah
confirm - pasti, pastikan, mengesahkan
conform - mematuhi
content - kandungan
control - kawal
copy - salin, menyalin
create - buat, cipta, wujud
create account - buka akaun
cron - cron (it refers to the program, proper noun)
current - semasa
custom - ubahsuaian, langgan
customization - pengubahsuaian
customize - ubahsuaikan
customized - diubahsuaikan
cut - potong
decrease - mengurangkan
default - lalai, sedia ada
define - mendefinisikan, mentakrifkan
delete - padam, hapus
delete account - tutup akaun
delimiter - pembatas
demote - turun aras, turunkan
deny - menafikan, tidak membenarkan
description - penerangan, huraian
descriptor - pemerihal
deselect - nyahpilih
designed - dirancang, direka
detail - perincian
deterrent - pencegahan
die - tamat
dimension - dimensi
directory - direktori
disable - nyahboleh
disabled - dinyahdayakan (used by google), dinyahbolehkan (see enabled), dilumpuhkan
disk - cakera
display - pamer (view - papar)
distribute - mengagih
distributed - teragih
distribution - agihan
document - dokumen
documentation - dokumentasi
dot - titik
download - muat turun
edit - sunting
effective - berkesan
element - unsur
e-mail - e-mel
enable - membolehkan
enabled - didayakan, dibolehkan
encoding - pengekodan
end - tamat
enter - masuk
entry - entri, pemasukan
environment - persekitaran
equivalent - sama, padanan, setara
error - ralat
escape - urutan keluar (use as in escape sequence)
escape sequence - urutan keluar
execute - laksana, jalankan
exit - keluar
expanded - terkembang
extended - dilanjutkan
extension - sambungan
external - luar, luaran
event - peristiwa
evaluate - menilai, tafsir
feature - ciri
feed - suapan (RSS feed = suapan RSS)
field - medan
file - fail
file extension - penyambung fail
file manager - pengurus fail
filename - nama fail
filter - tapis, tapisan
fine tune - memperbaiki, menjitukan
finish - tamat, berakhir, selesai
fixed width - keluasan tetap
fluid width - keluasan boleh berubah
folder - folder
footer - pengaki
for - untuk
for - bagi - lebih umum
fork - cabang
form - borang - mengandungi satu atau lebih medan
format - format
formatting - cara format
free tagging - tag bebas
freeform - bentuk bebas
front page - laman utama, muka depan/hadapan
form - borang
function - fungsi
general - umum (awam - public)
global - global
glossary - glosari
guide - panduan
guideline - garis panduan
handle - kendali
handler - pengelola
handbook - buku panduan
header - pengepala
help - bantuan
hidden - terlindung
hierarchy - hierarki
host - hos
hover - hover, mengepak-ngepak
hyperlink - hiperpautan
hyphen - tanda sempang
icon - ikon, arca
identification - pengecaman, pengenalpastian
if - jika
illegal - tak/tidak sah
image - imej
increase - naik, bertambah
indented - diengsot
index - indeks
input - input
insert - sisip
install - pasang
inter - antara (kata akar macam serupa?)
installation - pemasangan
interface - antara muka
item - butir, butiran
job - kerja
journal - jurnal
keep - simpan
keeping - menyimpan
keyword - kata kunci
label - label
latest - terbaru
launch - lancar
leave blank - tinggalkan tempat kosong, biarkan kosong
legacy - legasi
library - pustaka
limit - had, lingkungan, batas
line - pautan, talian
line break - pemisah barisan
link - pautan (see hyperlink)
list - senarai
load - muatan
local - setempat, lokal
locale - tempatan
log - log
login - log masuk
logout - log keluar
look and feel - rupa dan gaya
main - utama
maintain - mengekalkan, menyenggara
management - pengurusan
manual - manual
manually - secara manual
mapping - pemetaan
mask - topeng
match - padan
maximum - maksimum
member - ahli
memory - ingatan
message - mesej
method - kaedah, cara, metod
minimum - minimum
mode - mod
model - model
moderate - mengantara
moderation - pengantaraan
moderator - penyederhana, orang tengah
module - modul
move - alih
multiple - berbilang
multiple select - pilihan berbilang
n/a - n/a (as in not applicable, should not change as it is norm)
name - nama
navigation - panduan arah
next - selepas
node - nod
no - tiada
not - bukan
not applicable - tidak digunapakai
not well formed - tidak lengkap
number - nombor, bilangan
online - dalam talian
offline - luar talian
operation - pengendalian, operasi
optional - tidak wajib, pilihan
original - asli
out of date - sudah lapuk, ketinggalan zaman
outline - garis bentuk
overlap - bertindih
override - mengatasi, melangkau
padding - tambahan, selitan
page - halaman, laman; muka surat (buku)
page break - pemisah halaman
parent - induk
parse - menghuraikan, penghuraian
password - kata laluan
paste - tampal, pes
path - laluan
pattern - corak, paten
pending - tergantung, menanti
perform - dilaksana, dijalankan, dibuat
period - (tanda) noktah
periodic - berkala
permanent - kekal
permission - kebenaran, keizinan
phrase - frasa, ungkapan
pixel - piksel
platform - alat, wadah
policy - polisi
post - terbit, tampal, siar
post - terbitan, kiriman
posting - menyiarkan
posting - pencatatan, kiriman
preference - keutamaan
preprocessor - prapemproses
preview - pratonton
previous - sebelum
private - peribadi, persendirian
profile - profil
program - atur cara
promote - mempromosikan, naik aras
promoted - dipromosikan
property - sifat
provide - memberikan, menyediakan
public - awam (umum - general)
publish - terbit, menerbitkan
quotation mark - tanda petikan
queue - baris gilir, barisan
query - pertanyaan
quota - kuota
redirect - menghantar [sesuatu] ke alamat baru, melencongkan, mengalihkan
random - rawak
range - julat, sekitar
ranking - kedudukan, peringkat, penarafan
read more - baca lagi, baca lebih lanjut
real time - masa nyata
rebuild - membina semula
receive - menerima, mendapat
recent - terbaru
recently - baru-baru ini
recovery - pemulihan
recreate - diwujudkan semula
recursion - rekursi
redirect - menghantar [sst] ke alamat baru
reference - rujukan
referrer - perujuk
refine - menapis, memperhalus
register - daftar
regular - biasa, tetap, kerap
relational - relasional
relevance, relevancy - kaitan, hubungan
remote - jauh
remove - menyingkirkan, buang
rename - menamakan semula
replace - ganti
reply - balas, jawab
require - perlu
required - diperlukan
reserve - rizab, simpan
reset - set semula
resize - saiz semula
resolution - peleraian
restriction - had, sekatan
retain - kekal
retrieval - dapatan semula
return - kembali, balas
revision - penyemakan, semakan
revert - kembali menjadi, kembali/berbalik ke keadaan asal
rewrite - tulis semula
role - peranan
root - pangkal, akar  (<root> of taxonomy)
rule - peraturan
run - jalan, jalanan
schedule - menjadualkan
script - skrip
save - simpan
search - cari, carian
search engine - engin gelintar
secondary - sekunder
sequence - urutan, jujukan
server - pelayan
service - perkhidmatan, servis
set up - mengeset
setting - seting
sidebar - bar tepi
similar - serupa
site - laman, tapak
site-wide - seluruh laman
size - saiz
skip - langkau
slash - garis condong
software - perisian, sofwer
sort - isihan
source - sumber
spam - spam
specify - menentukan, menetapkan
start - mula
statistics - statistik
sticky - lekit
string - rentetan
subject - tajuk
submit - serah, masukkan
submission - penyerahan
support - sokongan
support - menyokong (supports extra features - boleh ada ciri-ciri lain)
sure - pasti
swap file - fail silih
syndicate - sindiket, disindiketkan
syndicated - disindiketkan
syndication - disindiketkan
synonym - sinonim
system - sistem
syntax - sintaks
table - jadual
tag - tag, tanda
taxonomy - taksonomi
teaser - pemikat, penggoda
template - templat, pencontoh
term - istilah
time out - tamat masa
then - maka
thesaurus - tesaurus
thread - urutan
threaded - berurutan
throttle - pendikit
title - tajuk, judul
toggle - togol
toolkit - peralatan
topic - topik, tajuk
track - jejak, kesan
trailing slash - garis condong di belakang
trimmed - disunting
truncate - memangkas, memendekkan
tweak - memulas
unblock - nyahsekat
underscore - garis bawah
undo - buat asal
uninstall - buang pemasangan
unpublished - tidak diterbitkan
up to date - terkini
update - kemaskini
upgrade - meningkatkan
upload - muat naik
uri - uri
url - url
user - pengguna
user-defined - takrifan pengguna
username - nama pengguna
** watchdog - penjaga (should not translated?)
web - lingkaran
web page - laman
website - laman web
weight - pemberat, penting
welcome - selamat datang
when - bila
while - selagi
wildcard - aksara bebas, kad bebas, pilihan rambang
work - berjaya, berkesan (as in won't work)
workflow - alir kerja
valid - sah
variable - pemboleh ubah
various - pelbagai
verify - sah betul
verification - penentusahan, menentusahkan
version - versi
view - papar (display - pamer), lihat
virtual - maya
visibility - darjah penglihatan
vocabulary - perbendaharaan kata
vote - undi
